package com.niceguy.flickrphotoviewer;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sejinpark on 2017. 2. 8..
 */

public class ImageLoaderConfig extends SimpleImageLoadingListener {

    // Variable for ImageLoader
    private static ImageLoaderConfig imageLoaderConfig = null;
    private DisplayImageOptions displayImageOptions = null;

    // Collection for downloaded images
    private static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

    /**
     * Return singletone instance
     *
     * @return
     */
    public static ImageLoaderConfig getInstance() {
        if (imageLoaderConfig == null) {
            Log.d(ImageLoaderConfig.class.getName(), "[getInstance]");
            imageLoaderConfig = new ImageLoaderConfig();
        }
        return imageLoaderConfig;
    }

    /**
     * Initialize options
     */
    public ImageLoaderConfig() {

        displayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public DisplayImageOptions getDisplayImageOptions() {
        return displayImageOptions;
    }

    public List getDisPlayedImages() {
        return displayedImages;
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        if (loadedImage != null) {
            ImageView imageView = (ImageView) view;
            imageView.setTag(imageUri);
            boolean firstDisplay = !displayedImages.contains(imageUri);
            if (firstDisplay) {
                FadeInBitmapDisplayer.animate(imageView, 500);
                displayedImages.add(imageUri);
            }
        }
    }

}
