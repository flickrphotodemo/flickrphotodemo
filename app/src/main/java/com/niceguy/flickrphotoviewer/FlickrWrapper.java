package com.niceguy.flickrphotoviewer;

import android.util.Log;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.REST;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by sejinpark on 2017. 2. 8..
 */

public class FlickrWrapper {

    // Flickr API keys
    private static final String API_KEY = "da4fadd0084ea1799ad33048f0d6a5c5";
    private static final String API_SEC = "186b04791439c326";

    private static FlickrWrapper flickrWrapper = null;

    /**
     * Return singletone instance
     *
     * @return
     */
    public static FlickrWrapper getInstance() {
        if (flickrWrapper == null) {
            Log.d(FlickrWrapper.class.getName(), "[getInstance]");
            flickrWrapper = new FlickrWrapper();
        }
        return flickrWrapper;
    }

    /**
     * Return the main instance of Flickr library
     * @return
     */
    public Flickr getFlickr() {
        try {
            Flickr f = new Flickr(API_KEY, API_SEC, new REST());
            return f;
        } catch (ParserConfigurationException e) {
            return null;
        }
    }

}
