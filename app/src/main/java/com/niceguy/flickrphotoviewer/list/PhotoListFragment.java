package com.niceguy.flickrphotoviewer.list;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.niceguy.flickrphotoviewer.R;

/**
 * Created by sejinpark on 2017. 2. 8..
 */

public class PhotoListFragment extends Fragment implements PhotoListAdapter.OnMoreListener {

    private RecyclerView recyclerView = null; // View for list
    private PhotoListAdapter photoListAdapter = null; // Adapter for photo list
    private PhotoListAsyncTask photoListAsyncTask = null; // Async request instance
    private int page = 0; // Value for photo list page, 10 photos per a page
    private int spancount = 3; // Span count for photolist
    private int rowcount = 10; // Row count for photolist

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(this.getClass().getName(), "[onCreateView]");

        // Initialize the main layout
        View view = inflater.inflate(R.layout.recyclerview_photolist, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview_photolist);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(spancount, StaggeredGridLayoutManager.VERTICAL));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(this.getClass().getName(), "[onActivityCreated]");

        // Initialize adapter
        photoListAdapter = new PhotoListAdapter(getActivity());
        photoListAdapter.setMoreListener(this);
        photoListAdapter.setRecyclerView(recyclerView);
        recyclerView.setAdapter(photoListAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();

        if( photoListAsyncTask != null ) { // exception for an unexpected situation
            if (photoListAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
                Log.d(this.getClass().getName(), "[onStop] photoListAsyncTask.cancel");
                photoListAsyncTask.cancel(true);
            }
            photoListAsyncTask = null;
        }
    }

    /**
     * Request photo list
     */
    public void requestPhotoList() {

        if( photoListAsyncTask == null ) { // When never executed
            Log.d(this.getClass().getName(), "[requestPhotoList] photoListAsyncTask.execute at the first time");
            photoListAsyncTask = new PhotoListAsyncTask(photoListAdapter);
            photoListAsyncTask.execute(++page, spancount * rowcount);
        } else if( photoListAsyncTask.getStatus() != AsyncTask.Status.RUNNING || photoListAsyncTask.getStatus() != AsyncTask.Status.PENDING ) { // When over one executed
            Log.d(this.getClass().getName(), "[requestPhotoList] photoListAsyncTask.execute");
            photoListAsyncTask = new PhotoListAsyncTask(photoListAdapter);
            photoListAsyncTask.execute(++page, spancount * rowcount);
        }
    }

    @Override
    public void onMore() {
        Log.d(this.getClass().getName(), "[onMore] requestPhotoList");
        requestPhotoList();
    }

}
