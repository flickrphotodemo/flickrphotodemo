package com.niceguy.flickrphotoviewer.list;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.niceguy.flickrphotoviewer.ImageLoaderConfig;
import com.niceguy.flickrphotoviewer.MainActivity;
import com.niceguy.flickrphotoviewer.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * Created by sejinpark on 2017. 2. 8..
 */

public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.ViewHolder> {

    private static int ITEMTYPE_PHOTO = 1; // Photo content
    private static int ITEMTYPE_MORE = 2; // Progress bar for more request

    // Variables
    private static Activity activity = null;
    private PhotoList photoList = null;
    private ImageLoaderConfig imageLoaderConfig;
    private ImageLoaderListener imageLoaderListener = new ImageLoaderListener();

    // More request
    private boolean isLoading = false;
    private OnMoreListener onMoreListener = null;

    /**
     * Constructor for adapter
     * @param activity
     */
    public PhotoListAdapter(Activity activity) {

        Log.d(this.getClass().getName(), "[PhotoListAdapter]");

        this.activity = activity;

        // Initialize
        photoList = new PhotoList();
        photoList.add(null);
        imageLoaderConfig = ImageLoaderConfig.getInstance();

        setHasStableIds(true); // Set fixed id for list item
    }

    /**
     * ViewHolder for list item
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imageView; // a small image
        private final TextView textView; // a title
        private Photo photo; // photo instance

        public ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Log.d(this.getClass().getName(), "[onClick] the item is clicked");

                    ((MainActivity)activity).onContent(photo, imageView);
                }
            });

            imageView = (ImageView) itemView.findViewById(R.id.imageview_photolist_item);
            textView = (TextView) itemView.findViewById(R.id.textview_photolist_item);
        }

        public ImageView getImageView() {
            return imageView;
        }
        public TextView getTextView() { return textView; }
        public void setPhoto(Photo photo) {
            this.photo = photo;
        }

    }

    /**
     * Interface for more request, called from PhotoListFragment
     */
    public interface OnMoreListener {

        /**
         * Called when request is need
         */
        void onMore();
    }

    /**
     * Interface for item click, called from MainActivity
     */
    public interface OnContentListener {

        /**
         * Called when item is clicked
         * @param photo
         * @param imageView
         */
        void onContent(Photo photo, ImageView imageView);
    }

    /**
     * Add new photo list
     * @param photoList
     */
    public void addPhotoList(PhotoList photoList) {

        if( photoList!= null ) { // exception when photoList is null
            Log.d(this.getClass().getName(), "[addPhotoList] new photoList.size() = " + photoList.size());

            if( this.photoList.size() > 0 ) { // remove a progress bar
                this.photoList.remove(this.photoList.size()-1);
            }
            this.photoList.addAll(photoList); // add photo list
            this.photoList.add(null); // add a progress bar
            notifyDataSetChanged();

            Toast.makeText(activity, activity.getString(R.string.photolist_size, this.photoList.size()-1), Toast.LENGTH_SHORT).show();
        }
        isLoading = false;

    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return photoList.get(position) == null ? ITEMTYPE_MORE : ITEMTYPE_PHOTO; // check whether it is a item or a progress bar
    }

    /**
     * Called when checking a item id, it is a fixed id for avoiding a refresh of the same image
     * @param position
     * @return
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.d(this.getClass().getName(), "[onCreateViewHolder] viewType = " + viewType);

        View view = null;

        if( viewType == ITEMTYPE_PHOTO ) { // when a photo item
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_photolist_item, parent, false);
        } else { // when a progress bar
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_photolist_item_progress, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Log.d(this.getClass().getName(), "[onBindViewHolder] position = " + position + ", holder = " + holder);

        Photo photo = photoList.get(position);
        if( photo != null ) { // check whether photo is null

            String url = (String)holder.getImageView().getTag(); // get a image link

            if( url == null || !url.equals(photo.getSmallUrl()) ) { // when adapter is refresh, avoiding a refresh of the same image
                holder.setPhoto(photo);
                holder.getImageView().setTag(photo.getSmallUrl());
                ImageLoader.getInstance().displayImage(photo.getSmallUrl(), holder.getImageView(), imageLoaderConfig.getDisplayImageOptions(), imageLoaderListener); // image downloader
                holder.getTextView().setText(photo.getTitle());
            }
        }
    }

    /**
     * Callback for more request, the listener will request photos
     * @param onMoreListener
     */
    public void setMoreListener(OnMoreListener onMoreListener) {
        this.onMoreListener = onMoreListener;
    }

    /**
     * Set recyclerview for more request
     * @param recyclerView
     */
    public void setRecyclerView(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if( !isLoading && photoList.size() != 1 ) { // check it is possible to request

                    int spancount = ((StaggeredGridLayoutManager)recyclerView.getLayoutManager()).getSpanCount();
                    int[] position = new int[spancount];
                    ((StaggeredGridLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPositions(position);
                    if( ( getItemCount() - recyclerView.getChildCount() <= ( position[0] + 1 ) ) ) { // check the bottom item is a progress bar

                        Log.d(this.getClass().getName(), "[setRecyclerView] onMoreListener.onMore");

                        isLoading = true;
                        onMoreListener.onMore(); // call request
                    }
                }

            }
        });
    }

    /**
     * ImageLoaderListener for adapter
     */
    class ImageLoaderListener extends SimpleImageLoadingListener {

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                imageView.setTag(imageUri);
                boolean firstDisplay = !imageLoaderConfig.getDisPlayedImages().contains(imageUri);
                if (firstDisplay) { // check it is the first, because of putting image data to a collection
                    Log.d(this.getClass().getName(), "[ImageLoaderListener:onLoadingComplete] imageView = " + imageView + ", imageUri = " + imageUri);

                    imageLoaderConfig.getDisPlayedImages().add(imageUri);

                }
            }
        }
    }

}
