package com.niceguy.flickrphotoviewer.list;

import android.os.AsyncTask;
import android.util.Log;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import com.niceguy.flickrphotoviewer.FlickrWrapper;

/**
 * Created by sejinpark on 2017. 2. 8..
 */

public class PhotoListAsyncTask extends AsyncTask<Integer, Void, PhotoList> {

    // Variables
    private PhotoListAdapter photoListAdapter = null;
    private int page = 0;

    public PhotoListAsyncTask(PhotoListAdapter photoListAdapter) {
        this.photoListAdapter = photoListAdapter;
    }

    @Override
    protected PhotoList doInBackground(Integer... integers) {

        Log.d(this.getClass().getName(), "[doInBackground] page = " + integers[0] + ", count = " + integers[1]);

        int page = integers[0];
        int count = integers[1];
        Flickr f = FlickrWrapper.getInstance().getFlickr();
        try {
            return f.getPhotosInterface().searchInterestingness(new SearchParameters(), count, page);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(PhotoList photos) {
        super.onPostExecute(photos);

        Log.d(this.getClass().getName(), "[onPostExecute] photos.size() = " + photos.size());

        photoListAdapter.addPhotoList(photos);
    }

    @Override
    protected void onCancelled(PhotoList photos) {

        Log.d(this.getClass().getName(), "[onCancelled]");

        photoListAdapter.addPhotoList(null); // exception for cancelled
    }

}
